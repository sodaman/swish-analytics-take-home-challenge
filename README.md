# Swish Analytics - Take Home Challenge

This is the response to the take home test for the Integration Engineer role. The purpose of this test was to evaluate attention to detail, knowledge of REST APIs and ability to create an API strategy.

> In the API you will find the players, games, teams, boxscores, and markets (player propositions) for the coming season.

> Evaluate this API and tell us what you see and what you think about it.

Within this repository you will find available:

* The orignal data file provided by Swish: `take_home_challenge_data0.txt`
* A visual representation of the data file: `Swish Challenge - Data Visual Representation v1.pdf`
* A ReadMe file documenting the process and results: `Swish Challenge - Readme.pdf`
* A Python script for reprocessing the data file: `datacheck1.py`
* The re-printed data is also available.
* A customer-geared API guide: `Swish Challenge - Customer Integration Guide.pdf`