import json

# The purpose of this file is to open a given JSON origin file and dump its contents back out in a cleaner format.

PATH_TO_FILE = "C:\\My Projects\\Swish Analytics\\"  # Path to the working data file (backslashes included)
DATAFILE = "take_home_challenge_data0"  # Name of the target file
EXTENSION = ".txt"


def main(data_reference):
    data_origin = "".join(data_reference)
    data_dest = data_reference[0] + data_reference[1] + "_reprint" + data_reference[2]

    with open(data_origin, "r") as f:
        contents = f.read()

    my_data_tree = json.loads(contents)

    # print("Dumping contents to file...")
    with open(data_dest, "w+") as f:
        json.dump(my_data_tree, f, indent=4)


if __name__ == "__main__":
    main([PATH_TO_FILE, DATAFILE, EXTENSION])
