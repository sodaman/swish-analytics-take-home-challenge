{
    "introduction": "Hi! This is the take home test for the Integration Engineer role. The purpose of this test is to evaluate your attention to detail, knowledge of REST APIs and ability to create an api strategy.",
    "backstory": "The NBA is building their player propositions API for the coming season and need help documenting, testing and guiding their strategy. Currently the API is in an alpha state, but will need to support many versions in the future. This API will be licensed to sportsbooks around the world so they can offer NBA betting to their customers.",
    "api": "In the API you will find the players, games, teams, boxscores, and markets (player propositions) for the coming season.",
    "spec": {
        "markets": {
            "path": "/players/markets",
            "attributes": [
                "playerId"
            ]
        },
        "boxscores": {
            "path": "/players/boxscores/:id",
            "attributes": []
        },
        "players": {
            "path": "/players/:id",
            "attributes": []
        },
        "teams": {
            "path": "/teams/:id",
            "attributes": []
        },
        "games": {
            "path": "/games",
            "attributes": []
        },
        "instructions": {
            "path": "/instructions",
            "attributes": []
        }
    },
    "tasks": [
        "Evaluate this API and tell us what you see and what you think about it.",
        "Create a set of documentation that clients could use to integrate the API.",
        "Create a visual representation of this API, the endpoints, the attributes, data types and anything else you deem relevant.",
        "What issues did you find, if any?",
        "What would you change in the API spec, if anything?",
        "How many hours do you think it would take for 1 developer to implement this? How would you speed up integration times?",
        "What endpoints could be added to facilitate an integration? How would these additional endpoints fit into the overall API strategy?",
        "If you have the time and ability, take a stab at ingesting the data. It does not have to run, but rather convey the point. Think of this task as an opportunity for extra credit"
    ],
    "deliverables": {
        "required": [
            "A written evaluation of the API answering all of the questions from the tasks array. Be as detailed and specific as possible.",
            "Your documentation of the API.",
            "A diagram mapping out the API, current endpoints, current attributes and their associated data types. Use this section to include any of your suggestions."
        ],
        "optional": [
            "Code demonstrating your implementation."
        ]
    }
}